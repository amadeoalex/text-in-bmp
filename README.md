
# TextInBMP

Simple program used to hide text data in the BMP image (Steganography)

## Project status

 - [x] Finished
 - [x] Pending rewrite (at some point in the future)

## Compilation

### Compiler used

g++ (x86_64-posix-seh-rev0, Built by MinGW-W64 project) 8.1.0

### Compile with

g++ main.cpp BMPImage.cpp -oTextToBMP.exe -std=c++17 -Wall -Wextra

## Usage

Parameters:
*  **-m** - mode (ENCRYPT/E or DECRYPT/D)
*  **-i** - input
*  **-o** - output
*  **-M** - MESSAGE

Currently the program has two modes (passing wrong arguments to the program causes it to print out basic instructions)

### Encryption

Takes input file and text content to combine them to output file.
It no output file is specified, it is generated automatically in the input file folder.

#### Example

Take catpic.bmp and embed the "meow" text into it to create hiddenmeow.bmp file.

TextToBMP.exe -m ENCRYPT -i catpic.bmp -o hiddenmeow.bmp -M meow

### Decryption

Takes input file and looks for encrypted text contents.

#### Example

Take hiddenmeow.bmp file from the encryption example and look for hidden text.

TextToBMP.exe -m DECRYPT -i hiddenmeow.bmp

## Authors

Paweł Wojtczak

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details.
