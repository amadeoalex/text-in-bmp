#include "BMPImage.h"

#include <bitset>
#include <iostream>

BMPImage::BMPImage(std::ifstream& fileStream)
{
    fileStream.unsetf(std::ios ::skipws);

    fileStream.seekg(0, std::ios_base::end);
    unsigned int fileSize = fileStream.tellg();
    printf("File size is: %u\n", fileSize);

    fileStream.seekg(0, std::ios::beg);

    imageData = std::vector<char>(fileSize);
    fileStream.read(imageData.data(), fileSize);

    if (fileSize > 0 && imageData[0] == 0x42 && imageData[1] == 0x4D) {
        pixelDataAddress = imageData[0x0A];
        printf("Pixel data starts at: %u(0x%02x)\n", pixelDataAddress, pixelDataAddress);

        charCapacity = (fileSize - (pixelDataAddress - 1)) / 8 - 2;
        printf("Available memory for text: %u\n", charCapacity);
    } else {
        throw std::runtime_error("Input file is not BMP file!");
    }

    fileStream.close();
}

void BMPImage::encodeChar(unsigned int address, const char c)
{
    std::bitset<8> charBits(c);

    for (unsigned int i = 0; i < charBits.size(); i++) {
        bool targetBit = (bool)charBits[i];

        if (targetBit)
            imageData[address + i] |= 1UL << 0;
        else
            imageData[address + i] &= ~(1UL << 0);
    }
}

bool BMPImage::encodeText(const std::string& message)
{
    if (message.size() > charCapacity) {
        printf("Error: message size is too long! Missing: %u\n", (unsigned int)message.size() - charCapacity);

        return false;
    }

    for (unsigned int i = 0; i < message.size(); i++) {
        encodeChar(pixelDataAddress + (i * 8), message[i]);
    }

    encodeChar(pixelDataAddress + message.size() * 8, '\r');
    encodeChar(pixelDataAddress + message.size() * 8 + 8, '\b');

    return true;
}

char BMPImage::decodeChar(unsigned int address)
{
    std::bitset<8> charBits;
    for (int i = 0; i < 8; i++) {
        charBits[i] = (imageData[address + i] >> 0) & 1U;
    }

    char c = (char)charBits.to_ulong();

    return c;
}

const std::string BMPImage::decodeText()
{
    std::vector<char> message;

    char lastChar = 0;
    unsigned int index = 0;
    while (true) {
        unsigned int checked = index * 8;
        if (checked > charCapacity) {
            puts("No message found in the image.");

            return "";
        }

        unsigned int startAddress = pixelDataAddress + index * 8;
        const char c = decodeChar(startAddress);

        if (lastChar == '\r' && c == '\b') {
            printf("End character found at index: %u\n", index);
            message.pop_back();
            break;
        }

        message.push_back(c);
        index++;

        lastChar = c;
    }

    const std::string messageString(message.begin(), message.end());

    return messageString;
}