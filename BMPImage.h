#ifndef BMPImage_H
#define BMPImage_H

#include <fstream>
#include <vector>

class BMPImage {
    unsigned int pixelDataAddress;
    unsigned int charCapacity;

    void encodeChar(unsigned int address, const char c);
    char decodeChar(unsigned int address);

  public:
    std::vector<char> imageData;

    explicit BMPImage(std::ifstream& fileStream);

    bool encodeText(const std::string& message);
    const std::string decodeText();
};

#endif