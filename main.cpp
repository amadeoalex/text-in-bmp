#include <iostream>
#include <optional>

#include "BMPImage.h"

void printInstructions()
{
    std::cout << "Encrypts text message into BMP files.\n"
              << "Usage:\n"
              << "FILE.EXE -m [MODE] -i [INPUT FILE] -o [OUTPUT FILE] -M [MESSAGE]\n"
              << "  [MODE]: ENCRYPT/E or DECRYPT/D\n"
              << "  [INPUT FILE]: used with both modes\n"
              << "  [OUTPUT FILE]: used with ENCRYPT mode\n"
              << "  [MESSAGE]: used with ENCRYPT mode\n\n"
              << "Example: FILE.exe -m E -i input.bmp -o output.bmp -M secret\n";
}

std::vector<std::string> getParams(int argc, char* argv[])
{
    std::vector<std::string> params;
    for (int i = 0; i < argc; i++) {
        params.push_back(std::string(argv[i]));
    }

    return params;
}

enum Mode {
    NONE,
    ENCRYPT,
    DECRYPT
};

bool encrypt(std::string& inputFile, std::string& outputFile, std::string& message)
{
    if (message.empty()) {
        puts("No message specified!");
        return false;
    }

    if (outputFile.empty()) {
        puts("Output file not specified, generating automatically");
        int dot = inputFile.find_last_of('.');
        std::string extension = inputFile.substr(dot, std::string::npos);
        std::string file = inputFile.substr(0, dot);

        outputFile = file + "OUTPUT" + extension;
    }

    std::ifstream fileStream(inputFile, std::ios::binary);

    if (!fileStream.good()) {
        puts("File access error!");
        return false;
    }

    if (!fileStream.is_open()) {
        puts("Cannot open file!");
        return false;
    }

    try {
        BMPImage image(fileStream);
        if (image.encodeText(message)) {
            std::ofstream outputStream(outputFile, std::ios::binary);
            if (!outputStream.good()) {
                puts("Output file access error!");
                return false;
            }

            if (!outputStream.is_open()) {
                puts("Cannot open output file!");
                return false;
            }

            outputStream.write(image.imageData.data(), image.imageData.size());
            outputStream.close();
        }
    } catch (std::exception& ex) {
        std::cout << ex.what() << '\n';
        return false;
    }

    return true;
}

std::optional<std::string> decrypt(std::string& inputFile)
{
    std::ifstream fileStream(inputFile, std::ios::binary);

    if (!fileStream.good()) {
        puts("File access error!");
        return {};
    }

    if (!fileStream.is_open()) {
        puts("Cannot open file!");
        return {};
    }

    try {
        BMPImage image(fileStream);
        return image.decodeText();
    } catch (std::exception& ex) {
        std::cout << ex.what() << '\n';
        return {};
    }
}

int main(int argc, char* argv[])
{
    std::vector<std::string> params = getParams(argc, argv);
    if (params.size() < 2) {
        printInstructions();
        return 0;
    }

    //TODO: change this to support text intput from file
    Mode mode = NONE;
    std::string inputFile;
    std::string outputFile;
    std::string message;

    //TODO: figure out it there is a better way to iterate through this vector (whilst knowing the index)
    for (int i = 0; i < (int)params.size(); i++) {
        std::string& param = params[i];
        std::string& nextParam = params[i + 1];

        if (param == "-m") {
            if (nextParam == "E" || nextParam == "ENCRYPT")
                mode = ENCRYPT;
            else if (nextParam == "D" || nextParam == "DECRYPT")
                mode = DECRYPT;
        } else if (param == "-i") {
            inputFile = nextParam;
        } else if (param == "-o") {
            outputFile = nextParam;
        } else if (param == "-M") {
            message = nextParam;
        }
    }

    if (inputFile.empty() || mode == NONE) {
        puts("Invalid use!");
        printInstructions();
        return 0;
    }

    if (mode == ENCRYPT) {
        if (encrypt(inputFile, outputFile, message)) {
            puts("Text encrypted.");
            return 0;
        } else {
            return 1;
        }
    } else if (mode == DECRYPT) {
        auto recoveredMessage = decrypt(inputFile);
        if (recoveredMessage.has_value())
            std::cout << "\nDecoded message is: '" << recoveredMessage.value() << "'\n";
        else
            return 1;
    }

    return 0;
}